# menu.sh

while true; do
    echo "Menu Interativo:"
    echo "1. Escolher customização"
    echo "2. Aplicar customização"
    echo "3. Restaurar para o prompt inicial"
    echo "4. Sair"

    read option

    case $option in
        1) source ./config.sh;;
        2) source ./apply.sh;;
        3) source ./reset.sh;;
        4) echo "Saindo do menu interativo"; exit 0;;
        *) echo "Opção inválida";;
    esac
done

