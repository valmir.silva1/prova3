# config.sh

echo "Escolha a customização desejada:"
echo "1. Mudança de cor"
echo "2. Exibir apenas o nome do usuário"
echo "3. Prompt em duas linhas"
echo "4. Sair"

read choice

case $choice in
    1) 
        echo "Escolha uma cor:"
        echo "1. Azul"
        echo "2. Verde"
        echo "3. Vermelho"
        read color_choice

        case $color_choice in
            1) PS1="\[\e[34m\]";;  # Azul
            2) PS1="\[\e[32m\]";;  # Verde
            3) PS1="\[\e[31m\]";;  # Vermelho
            *) echo "Opção inválida"; exit 1;;
        esac
        ;;
    2) PS1='\u$ ';;
    3) PS1='\[\e[0;36m\]\u@\h \w\n\[\e[0;32m\]\$ \[\e[0m\]';;
    4) echo "Saindo do menu"; exit 0;;
    *) echo "Opção inválida"; exit 1;;
esac

export PS1

